
angular.module('users.service', [])
  // .service('UsersService', UsersService);
  // services['UsersService'] = new UsersService(services['$http'])

  // .factory('UsersService', function ($http /* , config */) {
  //   const service = new UsersService($http)
  //   service.url = 'http://localhost:3000/' /* config.get('users_url') */
  //   return service;
  // })

  .provider('UsersService', function () {
    config = { url: '' }

    return { // UsersServiceProvider
      setUsersUrl(url) {
        config.url = url
      },
      $get($http) { // UsersService
        const service = new UsersService($http)
        service.url = config.url;
        return service;
      }
    }
  })



function UsersService($http) {
  this.url = ''

  this.fetchUsers = () => {
    return $http.get(this.url + '')
      .then(res => res.data)
  }

  this.fetchUserById = (id) => {
    return $http.get(this.url + `/${id}`).then(resp => resp.data)
  }

  this.saveUpdatedUser = (user) => {
    return $http.put(this.url + `/${user.id}`, user).then(resp => resp.data)
  }

  this.saveNewUser = (user) => {
    return $http.post(this.url + `/`, user).then(resp => resp.data)
  }

  this.deleteUserById = (id) => {
    return $http.delete(this.url + `/${id}`).then(resp => resp.data)
  }
}