const ticTacToe = angular.module('ticTacToe', []);

ticTacToe.controller('tttCtrl', function ($scope) {
    $scope.letter='';
    $scope.board = [];
    $scope.won = '';

    let marker = 'O';
    let moveCount = {};

    moveCount['X'] = 0;
    moveCount['O'] = 0;

    for(let x = 0; x < 3; x++){
        $scope.board[x] = [];
      }    

    $scope.nextTurn = function (x, y) {
        if ($scope.won === '') {
            if (marker === 'X') {
                marker = 'O';
            } else {
                marker = 'X';
            }
            $scope.board[x][y] = marker;
            moveCount[marker] += 1;
            if (moveCount[marker] >= 3) {
                if (checkForWin(x, y)) {
                    $scope.won = marker + ' Wygrał! Koniec gry';
                } else if ((moveCount['X'] + moveCount['O']) === 9) {
                    $scope.won = 'Remis!';
                }
            }
        }
    }

    $scope.reset = function() {
        for(let x = 0; x < 3; x++){
          for(let y = 0; y < 3; y++){
            $scope.board[x][y] = '';
          }
        }
        marker = 'O';
        $scope.won = '';
        moveCount['X'] = 0;
        moveCount['O'] = 0;
      }

    function checkRowColumn(x, y, check) {
        let flag = '';
        let row = x;
        let column = y;
        for (let i = 0; i < 3; i++) {
            if (check === 'row') {
                column = i;
            } else {
                row = i;
            }
            if ($scope.board[row][column] !== marker) {
                flag = false;
            }
        }
        if (flag === '') {
            flag = true;
        }
        return flag;
    }

    function checkDiagonals() {
        if (($scope.board[0][0] === marker &&
            $scope.board[1][1] === marker &&
            $scope.board[2][2] === marker) ||
            ($scope.board[0][2] === marker &&
            $scope.board[1][1] === marker &&
            $scope.board[2][0] === marker)) {
            console.log($scope.board[0][0], $scope.board[2][2],
                $scope.board[0][2], $scope.board[2][0]);
            return true;
        }
        return false;
    }

    function checkForWin(x, y) {
        if (checkRowColumn(x, y, 'column')) {
            return true;
        } else if (checkRowColumn(x, y, 'row')) {
            return true;
        } else if (checkDiagonals()) {
            return true;
        } else {
            return false;
        }
    }

})