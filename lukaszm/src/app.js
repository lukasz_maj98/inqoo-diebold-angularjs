angular.module('config', [])
  .constant('PAGES', [
    { name: 'users', label: 'Users' },
  ])

const app = angular.module('myApp', [
  'config', 'users','tasks'
])

app.config(function (PAGES) {
  console.log(PAGES)
})
app.run(function ($rootScope, PAGES) { 

})

app.controller('AppCtrl', ($scope, PAGES) => {
  $scope.title = 'MyApp'
  $scope.user = { name: 'Guest' }

  $scope.isLoggedIn = false;

  $scope.login = () => {
    $scope.user.name = 'Admin'
    $scope.isLoggedIn = true
  }

  $scope.logout = () => {
    $scope.user.name = 'Guest'
    $scope.isLoggedIn = false
  }

  $scope.pages = PAGES

  $scope.currentPage = $scope.pages[0]

  $scope.goToPage = pageName => {
    $scope.currentPage = $scope.pages.find(p => p.name === pageName)
  }
})
