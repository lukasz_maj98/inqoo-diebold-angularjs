const tasks = angular.module('tasks', [])

tasks.constant('INITIAL_TASKS', [])
tasks.constant('INITIAL_MODE', 'show')
tasks.controller('TasksCtrl', (
  $scope,
  INITIAL_TASKS,
  INITIAL_MODE) => {

  console.log('Hello TasksCtrl', { ...$scope })

  $scope.mode = INITIAL_MODE
  $scope.tasks = INITIAL_TASKS
  $scope.filtered = []

  $scope.draft = {}

  $scope.selectedTask = {
    id: "123",
    name: "Task 123",
    completed: true
  }


  $scope.query = ''
  $scope.changeQuery = () => {
    $scope.filtered = $scope.tasks.filter(
      t => t.name.toLocaleLowerCase().includes($scope.query.toLocaleLowerCase())
    )
  }

  $scope.counts = { completed: 0, total: 0 }
  $scope.updateCounts = () => {
    $scope.counts = $scope.tasks.reduce((counts, task) => {
      counts.total += 1;
      if (task.completed) { counts.completed += 1 }
      return counts
    }, { completed: 0, total: 0 })
  }


  $scope.$watch('tasks', (newVal, oldVal) => {
    console.log(' task watcher update');
    $scope.changeQuery('')
    $scope.updateCounts()
  })


  $scope.edit = () => {
    $scope.mode = 'edit'
    $scope.draft = { ...$scope.selectedTask }
  }

  $scope.cancel = () => {
    $scope.mode = 'show'
  }

  $scope.toggleCompleted = (draft) => {
    draft.completed = !draft.completed
    $scope.tasks = $scope.tasks.map(
      task => task.id == draft.id ? draft : task
    )
  }

  $scope.save = () => {
    $scope.mode = 'show'
    $scope.selectedTask = { ...$scope.draft }
    $scope.tasks = $scope.tasks.map(
      task => task.id == $scope.draft.id ? $scope.draft : task
    )
  }

  $scope.addTask = () => {
    $scope.tasks = [...$scope.tasks, { 
      id: Date.now().toString(),
      name: $scope.fastTaskName, completed: false
    }]
    $scope.fastTaskName = ''

  }

  $scope.remove = (id) => {
    $scope.tasks = $scope.tasks.filter(task => task.id !== id)

  }

  $scope.select = (item) => {
    $scope.selectedTask = item
  }
})




/*
const tasks = angular.module('tasks', []);



tasks.controller('TasksCtrl', ($scope) => {

  $scope.findItem = ""

    $scope.mode = "show"
    $scope.tasks = [{
        id: "123",
        name: "Naleśniki",
        completed: true
    },
    {
        id: "234",
        name: "Placki",
        completed: false
      },{
        id: "345",
        name: "Chleb",
        completed: true
      }];
    
      $scope.draft = {}
      $scope.task = $scope.tasks[0]
    
    $scope.edit = () => {
        $scope.mode = "edit"
        $scope.taskEdit = {...$scope.task}

    }
    
    $scope.cancel = () =>{
        $scope.mode = "show"
    }
    $scope.save = () =>{
        $scope.mode = 'show'
        $scope.task = {...$scope.taskEdit}
        const index = $scope.tasks.findIndex(i => i.id === $scope.task.id);
        Object.assign($scope.tasks[index], $scope.task);
    }
    $scope.select = (item) => {
      
      $scope.task = $scope.tasks.find(i=>
        i.id === item)
        $scope.mode = "show"
    }
    $scope.remove = (item) =>{

      $scope.task = item
      $scope.tasks = $scope.tasks.filter(i => i != $scope.task);
      $scope.mode = "show"

      /*tasks.task.splice(item, 1)
    }

    $scope.filter = (item) =>{

      $scope.filtered = $scope.tasks.filter(i  =>  i.name.toLowerCase().includes(item.toLowerCase()))
      


    }

    $scope.filter('')
    

})

*/