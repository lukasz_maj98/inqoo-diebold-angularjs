
const users = angular.module('users', ['users.service'])



angular.module('users').controller('UsersPageCtrl', ($scope, UsersService) => {
  const vm = $scope.usersPage = {}
  vm.draft
  vm.selected = null

  vm.select = (id) => {
    return UsersService.fetchUserById(id).then(user => {
      vm.selected = user
      vm.mode = 'details'
    })
  }

  vm.edit = () => {
    vm.mode = 'edit'
    vm.draft = { ...vm.selected }
  }

  vm.refresh = () => {
    return UsersService.fetchUsers()
    .then(user => {
      vm.selected = user
      
    })
  }
  vm.refresh()

  vm.save = (draft) => {
    UsersService.saveEditedUseToDb(draft)
      .then(saved => {
        return $q.all([
          vm.select(saved.id),
          vm.refresh()
        ])
      })

  }



})



/*
  .controller('UserListCtrl', ($scope) => {
    const vm = $scope.list = {}
    vm.filtered = []
    vm.select = (id) => vm.onSelect(id)

  })

  .controller('UserDetailsCtrl', ($scope) => {
    const vm = $scope.details = {}
    vm.user = null

  })

  .controller('UserEditFormCtrl', ($scope) => {
    const vm = $scope.editform = {}
    vm.draft = {}
    
  })
*/

/*
users.controller('UsersCtrl', ($scope) => {

    $scope.users = [
        { id: "123", username: 'Admin' }
      ]

      $scope.visable = 'task'

    $scope.otherSite = (itemUser) =>{
        $scope.visiable = itemUser
    }

})

*/