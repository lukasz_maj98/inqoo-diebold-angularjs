
angular
.module('myApp')
.constant('PAGES', [
  
  { name: 'tasks', label: 'Tasks', tpl: 'views/tasks-view.tpl.html' },
  { name: 'users', label: 'Users', tpl: 'views/users-view.tpl.html' },
  { name: 'settings', label: 'Settings', tpl: 'views/settings-view.tpl.html'}
])
.constant('INITIAL_TASKS', [{
  id: "123",
  name: "Task 123",
  completed: true
}, {
  id: "234",
  name: "Task 234",
  completed: false
}, {
  id: "345",
  name: "Task 345",
  completed: true
}])


/*
angular.module('myApp', ["tasks", "users"]);
const app = angular.module('myApp')


app.run(function($rootScope){
    $rootScope.title = 'my new App';
});


app.controller('AppCtrl', ($scope) => {

    $scope.title = "myApp"
    $scope.user = {name: "Guest"}

    $scope.isLoggedIn = false;
    $scope.login = () => {
        $scope.user.name = 'Admin';
        $scope.isLoggedIn = true
    }
    $scope.logout = () => {
        $scope.user.name = 'Guest';
        $scope.isLoggedIn =false
    }
    
})
*/