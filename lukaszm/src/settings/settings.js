
const users = angular.module('settings', ['settings.service'])

angular.module('settings').controller('SettingsPageCtrl', ($scope, SettingsService) => {
  const vm = $scope.settingsPage = {}

  vm.selected = null
  vm.users = SettingsService.getSettings()


})