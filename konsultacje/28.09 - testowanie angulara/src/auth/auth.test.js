describe('Auth Register Ctrl', () => {

  beforeEach(module('app'));

  var $controller, $rootScope, myScope;

  beforeEach(inject(function (_$controller_, _$rootScope_) {
    // The injector unwraps the underscores (_) from around the parameter names when matching
    $controller = _$controller_;
    $rootScope = _$rootScope_;
    myScope = $rootScope.$new()
    $controller('RegisterCtrl', { $scope: myScope })
  }));


  it('should check password strength', () => {

    expect(myScope.message).toBe('')

    myScope.password = 'abc'
    myScope.register()
    expect(myScope.message).toEqual('Weak password', 'Test short password')

    myScope.password = 'Abcde'
    myScope.register()
    expect(myScope.message).toEqual('Medium password', 'Test simple password')

    myScope.password = 'Abcde5'
    myScope.register()
    expect(myScope.message).toEqual('Strong password', 'Test simple password')

    // myScope.password = 'Abcd5'
    // myScope.register()
    // expect(myScope.message).toEqual('Strong password')
  })

})

describe('Password meter', () => {
  var $compile,
    $rootScope;

  // Load the myApp module, which contains the directive
  beforeEach(module('app.auth'));

  // Store references to $rootScope and $compile
  // so they are available to all tests in this describe block
  beforeEach(inject(function (_$compile_, _$rootScope_) {
    // The injector unwraps the underscores (_) from around the parameter names when matching
    $compile = _$compile_;
    $rootScope = _$rootScope_;
  }));

  it('shows empty meter', () => {

    $rootScope.level = 20

    // Compile a piece of HTML containing the directive
    var element = $compile(`<password-meter value="level"></password-meter>`)($rootScope);
    // fire all the watches, so the scope expression {{1 + 1}} will be evaluated
    $rootScope.$digest();
    // Check that the compiled element contains the templated content

    // expect(element.find('.progress-bar').css('width')).toEqual('20%')
    expect(element[0].querySelector('.progress-bar').style.width).toEqual('20%')
    expect(element[0].querySelector('.progress').className).toMatch('text-danger')


    $rootScope.level = 80
    $rootScope.$digest();
    expect(element[0].querySelector('.progress-bar').style.width).toEqual('80%')
    expect(element[0].querySelector('.progress').className).toMatch('text-success')
   
  })

})
