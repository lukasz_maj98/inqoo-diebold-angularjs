angular.module('app.auth', [])
  .controller('RegisterCtrl', function ($scope) {

    $scope.username = ''
    $scope.password = ''
    $scope.register = () => {
      if ($scope.password.match(/(?=.*\d)(?=.*[a-z])(?=.*[A-Z])/)) {
        $scope.message = 'Strong password'
      } else if ($scope.password.length >= 5) {
        $scope.message = 'Medium password'
      } else {
        $scope.message = 'Weak password'
      }
    }
    $scope.message = ''

  })

  .directive('passwordMeter', function () {
    return {
      restrict: 'E',
      scope: { value: '=' },
      template: /* html */`<div class="progress"
      ng-class="{
        'text-danger': value < 21,
        'text-success': value < 81, 
      }">
        <div class="progress-bar" role="progressbar"  
          style="width: {{value}}%;"></div>
      </div>
      `
    }
  })